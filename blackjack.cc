// blackjack.cc

#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Implement this function.
int BlackJack(const vector<string>& cards){
	int sum=0, a_num=0,i;
//	vector<string>::iterator it;

	for(i=0; i<(int)cards.size();  i++)
	{
		if(cards[i].size()==1 &&   '0'<cards[i][0]&&cards[i][0]<='9')
			sum+=cards[i][0]-'0';
		else if(cards[i]=="10" || cards[i]=="K" || cards[i]=="Q" || cards[i]=="J")
			sum+=10;
		else if (cards[i]=="A")
			sum+=1,a_num+=1;
		else
			return -1;
	}
	while(sum<=11 && 0<a_num)
	{
		sum	+=10;
		a_num	-=1;
	}
	if(21<sum)
		sum=0;
	return sum;
}

int main() {
  while (true) {
    vector<string> cards;
    int num_cards;
    cin >> num_cards;
    if ((cin.rdstate() & istream::failbit) != 0) break;
    cards.resize(num_cards);
    for (int i = 0; i < num_cards; ++i) cin >> cards[i];

    int ret = BlackJack(cards);
    if (ret < 0) break;
    else if (ret == 0) cout << "Exceed" << endl;
    else if (ret == 21) cout << "BlackJack" << endl;
    else cout << ret << endl;
  }
  return 0;
}
