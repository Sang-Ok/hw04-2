// vending_machine.cc
#ifndef _VENDING_MACHINE_CC_
#define _VENDING_MACHINE_CC_
#include "vending_machine.h"
#include <iostream>
using namespace std;


void VendingMachine::AddBeverage(const string& name, int unit_price, int stock){
//	BeverageInfo data(name, unit_price, stock);
	BeverageInfo data={name, unit_price, stock};
	beverages_[name]=data;  // ==  beverages_.insert(pair<name,data>);
}

int VendingMachine::Buy(const map<string,int>& items, int money){
	int sum=0;
	map<string, int>::iterator it1, it2;
	for(it1 = items.begin();  it1 != items.end();  ++it1){//그리고 왜! 왜 이 =연산이 불가능한거지? 컴파일러 이놈은 iterator가 이런 연산이 불가능하다고 하는데... 젠장 난 분명이 c++ reference랑 똑같이 했단 말이다.
		it2 = beverages_.find(it1->first);
		if(it2 == beverages_.end())
			return -3;		//UNKNOWN ITEM
		else if(it2->second.stock  < it1->second)
			return -2;		//OUT OF STOCK
		else
			sum += (it2->second.unit_price_)*(it1->second);
	}
	if(money<sum)
		return -1;
	for(it1=items.begin();  it1 != items.end();  it1++){
		it2 = beverages_.find(it1->first);
		it2->second.stock  -= it1->second;
	}
	return (money-sum);
}



#endif//_VENDING_MACHINE_CC_
