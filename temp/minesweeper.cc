// minesweeper.cc
#ifndef _MINESWEEPER_CC_
#define _MINESWEEPER_CC_
#define TEST(str1,str2)		cout<<"\t\t"<<str1<<str2<<endl
#define TES3(str1,str2,str3)	cout<<"\t\t"<<str1<<str2<<str3<<endl
using namespace std;


bool Minesweeper::inRange(const int x, const int y)const{
	if(x<0 || w_<=x || y<0 || h_<=y)return false;
	if(map_[y][x]=='*')		return true;
	else				return false;
}

void Minesweeper::sweep(const int x, const int y){
	int i,j;
	//TES3("(x,y)==",x,y);
	for(i=-1; i<=+1; i++)
		for(j=-1; j<=+1; j++)
			if(inRange(x+i,y+j))
				map_[y][x]+=1;
	//TEST("value : ", map_[y][x]);
}
void Minesweeper::deSweep(const int x, const int y){
	int i,j;
	for(i=-1; i<=+1; i++)
		for(j=-1; j<=+1; j++)
			if(inRange(x+i,y+j))
				map_[y+j][x+j]-=1;
	//TEST("value : ", map_[y][x]);
}


bool Minesweeper::SetMap(const size_t w, const size_t h, const vector<string>& map){
	int i,j;
	//TEST("GET INT TO SetMap()","");
	//TEST("W:=",w);
	//TEST("H:=",h);
	w_=w, h_=h;
	map_.resize(h);
	if(map.size()!=h)	return false;
	for(i=0; i<h; i++){
		if(map[i].size()!=w)return false;
		map_.clear();
		for(j=0; j<w; j++)
			if (map[i][j]=='.')	map_[i]+='0';
			else if (map[i][j]=='*')map_[i]+='*';
			else	return false;
	}
	//TEST("FINISHING : ","map=map_");
	{
	  const int a = w_, b = h_;
	  for (int y = 0; y < b; ++y) {
	    for (int x = 0; x < a; ++x) cout << map_[y][x];
	    cout << endl;
	  }
	}//삭제할부분




	for(i=0; i<h; i++)
		for(j=0; j<w; j++)
			if(map_[i][j]!='*')
				sweep(j,i);
	return true;
}

bool Minesweeper::ToggleMine(int y, int x){//뭔생각으로 :Toggle만 (x,y)가 아니라 (y,x)로 했는지 모르겠습니다.
	int i,j;
	TES3("GET IN TO ToggleMine(",x,y);
	if(map_[y][x]=='*'){	
		//TEST("CASE :\t map_[y][x] == ",map_[y][x]);
		map_[y][x]='0';
		//TEST("DONE :\t map_[y][x] == ",map_[y][x]);
		sweep(x,y);			//sweep()과 같은역할이었네...
/*		for(i=y-1; i<=y+1; i++)
			for(j=x-1; j<=x+1; j++)
				if(x<0 || w_<x || y<0 || h_<y)
					continue;
				else if(map_[i][j]=='*')
					map_[y][x]+=1;
*/
	}
	else{
		//TEST("CASE :\t map_[y][x] == ",map_[y][x]);
		map_[y][x]='*';
		//TEST("DONE :\t map_[y][x] == ",map_[y][x]);
		for(i=y-1; i<=y+1; i++)
			for(j=x-1; j<=x+1; j++)
				if(j<0 || w_<j || i<0 || h_<i)
					continue;
				else if(map_[i][j]!='*')
					map_[i][j]+=1;
	}
	return true;
}

#endif
