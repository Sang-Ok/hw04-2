// minesweeper.h

#ifndef _MINESWEEPER_H_
#define _MINESWEEPER_H_

#include <string>
#include <vector>
using namespace std;

class Minesweeper {
 public:
  Minesweeper(){w_=h_=0;}
  ~Minesweeper(){;}

  bool SetMap(const size_t w, const size_t h, const std::vector<std::string>& map);
  bool ToggleMine(int x, int y);

  size_t width() const{return w_;}
  size_t height() const{return h_;}
  char get(const int x, const int y) const{return map_[y][x];};
  void sweep(const int x, const int y);
  void deSweep(const int x, const int y);
  bool inRange(const int x, const int y)const;
 private:
 size_t w_,h_;
 vector<string> map_;
  // Add private member variables and functions as needed.

};

#endif  // _MINESWEEPER_H_
